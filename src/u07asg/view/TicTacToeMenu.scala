package u07asg.view

import u07asg.{TicTacToeApp, TicTacToeImpl}

import scala.swing._

/**
  * Created by Manuel Bottax on 25/04/2017.
  */
class TicTacToeMenu() extends MainFrame {

  val classicButton: Button = new Button("Classic")
  val misereButton: Button =  new Button ("Misere")
  val ultimateButton: Button = new Button("Ultimate")

  title = "Tic Tac Toe 2.0.0"
  resizable = false
  preferredSize = new Dimension(300, 125)
  contents = new GridPanel(2, 1) {
    val l = new Label("- Select Game Version -")
    l.horizontalAlignment = Alignment.Center
    contents += l
    contents += new FlowPanel() {
      contents += classicButton
      contents += misereButton
      contents += ultimateButton
    }

    border = Swing.EmptyBorder(10, 10, 10, 10)
  }

  listenTo(classicButton)
  listenTo(misereButton)
  listenTo(ultimateButton)

  import scala.swing.event._
  reactions += {

    case ButtonClicked(`classicButton`) => {
      println("Classic selected !")
      try new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"))
      catch {
        case ex: Exception =>
          System.out.println("Problems loading the theory")
      }
      visible = false
    }

    case ButtonClicked(`misereButton`) => {
      println("Misere selected !")
      try new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"))
      catch {
        case ex: Exception =>
          System.out.println("Problems loading the theory")
      }
      visible = false
    }

    case ButtonClicked(`ultimateButton`) => {
      println("Ultimate TTT selected !")
      try new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"))
      catch {
        case ex: Exception =>
          System.out.println("Problems loading the theory")
      }
      visible = false
    }

  }

  visible = true

}



  object TicTacToeMenu {
    def main(args: Array[String]) {
       new TicTacToeMenu
    }
  }

